import pydim
import signal
import time
import sys
import multiprocessing
from collections import deque

exit = multiprocessing.Event()
dimqueue = multiprocessing.Queue()

services = {
    "SOB": "NA62/Timing/SOB",
    "EOB": "NA62/Timing/EOB",
    "InRun": "RunControl/InRun",
    "Run": "RunControl/RunNumber",
    "Burst": "RunControl/BurstNumber",
    "T10": "RunControl/T10",
    "ARGONION": "RunControl/Argonion",
    "BBQ" : "NA62PrimitiveIRC/BBQ_IRC",
    "L0plus": "l0Server_plus/CounterLTU",
    "L0minus": "l0Server/TriggerNumber",
    "L1": "RunControl/L1TriggersNumber",
    "L2": "RunControl/L2TriggersNumber",
}

tels = [
    "ANTI0",
    "CEDAR0",
    "CEDAR1",
    "CEDAR2",
    "CEDAR3",
    "CEDAR4",
    "CEDAR5",
    # "CHANTI1",
    # "CHANTI2",
    # "CHANTI3",
    "CHOD",
    "HAC",
    "IRC",
    "LAV1",
    "LAV10",
    "LAV11",
    "LAV12",
    "LAV2",
    "LAV3",
    "LAV4",
    "LAV5",
    "LAV6",
    "LAV7",
    "LAV8",
    "LAV9",
    "MUV3",
    "RICH1",
    "RICH2",
    "RICH3",
    "RICH4",
    "RICH5",
]

data = list(services.keys()) + ['CHOKE', 'BADDATA']

class DimClient:
    def __init__(self, dns:str, services: dict) -> None:
        
        pydim.dic_set_dns_node(dns)
        for label, service in services.items():
            pydim.dic_info_service(service, self._put_to_queue(label), pydim.MONIT_ONLY)
        try:
            exit.wait()
        except (KeyboardInterrupt, SystemExit):
            pass
    
    def _put_to_queue(self, label: str):
        def callback(payload):
            dimqueue.put((label, payload))
            if DEBUG:
                print(label, payload)
        return callback


class BurstReader:
    def __init__(self, outfilename) -> None:
        self.bursts = deque()
        self.outfilename = outfilename
        try:
            while not exit.is_set():
                self.receive(*dimqueue.get())
        except (KeyboardInterrupt, SystemExit):
            self.flush()

    def receive(self, label, payload):
        if 'SOB' in label:
            self.receive_sob(payload)
        elif 'L1' in label or 'L2' in label:
            self.receive_hlt(label, payload)
        else:
            self.receive_info(label, payload)
    
    def receive_sob(self, sob_timestamp):
        self.flush() # save old bursts
        self.bursts.append({}) # make new burst
        self.receive_info('SOB', sob_timestamp)
    
    def receive_hlt(self, label, value):
        if not len(self.bursts) >= 2:
            return
        self.bursts[-2][label] = value
    
    def receive_info(self, label, payload):
        if not self.bursts:
            return
        if label in self.bursts[-1]:
            print(f"WARNING: overwriting {label} {self.bursts[-1][label]} with {payload}")
            self.bursts[-1]['BADDATA'] = 1
        self.bursts[-1][label] = payload
        
    def flush(self):
        '''
        Flush self.burst: save all ready bursts, then all but the latest non-ready bursts
        (ready = not in run or in run and hlt info received)
        '''
        if not self.bursts:
            return
        while self.bursts:
            burst: dict = self.bursts.popleft()
            if self.is_ready(burst): # ready: save it
                self.save(burst)
            elif self.bursts and any(self.is_ready(b) for b in self.bursts): # there are other ready bursts: process those first
                self.bursts.append(burst)
            elif self.bursts or exit.is_set(): # no more ready bursts to process, but we already have other old bursts, or we are exiting and we save whatever this is
                self.save(burst)
            else: # no other bursts, and this is a non-ready burst
                self.bursts.appendleft(burst) # put it back! want to keep it
                break
            
        
    def is_ready(self, burst: dict):
        try:
            if burst['InRun'] and 'L1' not in burst or 'L2' not in burst:
                return False
        except KeyError:
            if 'BADDATA' not in burst:
                if not exit.is_set():
                    print("WARNING: No InRun information")
                burst['BADDATA'] = 1
        return True

    def pre_process(self, burst: dict):
        # CHOKE
        if 'CHOKE' in burst:
            print("ERROR! CHOKE mask already present!")
        burst['CHOKE'] = 0
        for i, tel in enumerate(tels):
            try:
                burst['CHOKE'] |= (burst.pop(f'CHOKE {tel}') << i)
            except KeyError:
                if not exit.is_set():
                    print(f"WARNING: no choke data from {tel}")
                burst['BADDATA'] = 1
            
        # rounding floats
        for key, rounding in (('T10', 4), ('BBQ', 2)):
            try:
                burst[key] = round(burst[key], rounding)
            except KeyError:
                if not exit.is_set():
                    print(f"WARNING: no {key} data")
                burst['BADDATA'] = 1

        if 'BADDATA' not in burst and not exit.is_set():
            burst['BADDATA'] = 0
        
        return burst
    
    def save(self, burst: dict):
        burst = self.pre_process(burst)
        with open(self.outfilename, 'a') as outfile:
            outfile.write(','.join(str(burst.get(field, '')) for field in data))
            outfile.write('\n')
        print(', '.join(f'{k}: {v}' for k,v in burst.items()))


if __name__ == '__main__':
    DEBUG = '--debug' in sys.argv
    if DEBUG: sys.argv.remove('--debug')
    
    try:
        outfilename = sys.argv[1]
    except IndexError:
        outfilename = f'{int(time.time())}.csv'
    with open(outfilename, 'w') as outfile:
        outfile.write(",".join(data))
        outfile.write('\n')

    p1 = multiprocessing.Process(target=DimClient, args=('na62dimserver', services))
    p2 = multiprocessing.Process(target=DimClient, args=('na62lkrl0', {f"CHOKE {tel}" : f"TEL_{tel}/ChokeInfo" for tel in tels}))
    reader = multiprocessing.Process(target=BurstReader, args=(outfilename,))
    p1.start()
    p2.start()
    reader.start()

    def graceful_exit(sig, frame):
        print("\nReceived SIGINT. Dumping all unfinished data and exiting...")
        exit.set()
        p1.join()
        p2.join()
        reader.join()
        
    signal.signal(signal.SIGINT, graceful_exit)
    signal.pause()
