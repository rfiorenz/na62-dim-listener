TEL62 = [
    "ANTI0",
    "CEDAR0",
    "CEDAR1",
    "CEDAR2",
    "CEDAR3",
    "CEDAR4",
    "CEDAR5",
    # "CHANTI1",
    # "CHANTI2",
    # "CHANTI3",
    "CHOD",
    "HAC",
    "IRC",
    "LAV1",
    "LAV10",
    "LAV11",
    "LAV12",
    "LAV2",
    "LAV3",
    "LAV4",
    "LAV5",
    "LAV6",
    "LAV7",
    "LAV8",
    "LAV9",
    "MUV3",
    "RICH1",
    "RICH2",
    "RICH3",
    "RICH4",
    "RICH5",
]

_detectors = set(tel.strip('0123456789') if tel not in ('ANTI0', 'MUV3') else tel for tel in TEL62)
_masks = {det: sum((1 << i) for i, tel in enumerate(TEL62) if tel.startswith(det)) for det in _detectors}


def read_csv(filepath_or_buffer, *, rescale_argonion=True, rescale_T10=False, fill_runnr=True, **kwargs):
    import pandas as pd
    df = pd.read_csv(filepath_or_buffer, **kwargs)
    if rescale_argonion:
        df['ARGONION'] *= 1e4
    if rescale_T10:
        df['T10'] *= 1e11
    if fill_runnr:
        df['Run'].ffill(inplace=True) # fill all run numbers forwards
        fvi = df['Run'].first_valid_index()
        if fvi is not None and fvi > 0:
            df.loc[fvi - 1, 'Run'] = df.loc[fvi, 'Run'] - 1  # calculate last NaN run number  = first available run number - 1
            df['Run'].bfill(inplace=True)  # backfill the first run number
    return df

def chokes(chokemask, detectors=False):
    if detectors:
        return {detector for detector, detmask in _masks.items() if chokemask & detmask}
    else:
        return {tel for i, tel in enumerate(TEL62) if chokemask & (1 << i)}
    
def is_choking(chokemask, tel62):
    if isinstance(tel62, str):
        return bool(chokemask & (1 << TEL62.index(tel62)))
    else:
        return any(chokemask & (1 << i) for i, tel in enumerate(TEL62) if tel in tel62)